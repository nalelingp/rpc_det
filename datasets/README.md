Please make symlink of retail-product-checkout dataset to this folder, then copy `rpc.yaml` into that folder.
```bash
# format:
# $ ln -s <DIR_DATASET> ./rpc

# example:
# assume that unzipped dataset `retail_product_checkout` locates under the folder `/data/dataset/rpc`
$ ln -s /data/datasets/rpc/ ./rpc
```


You can download and extract the dataset to any location you want, and you can also use the script
`unzip_dataset.sh` under the folder `rpc_det/scripts` to create desired folder structure automatically.

To run the script `unzip_dataset.sh`, remember copy it to the same folder where the zip file of
dataset locates before running it.

Desired folder structure:
```
rpc
├── retail_product_checkout
│   ├── images
│   ├── instances_test2019.json
│   ├── instances_train2019.json
│   ├── instances_val2019.json
│   └── labels    # this folder does not exist in orignal zip file, it will be created
│                 # after running `rpc_det/scripts/format_label_rpc.sh`
├── unzip_dataset.sh    # this script is copied from `rpc_det/scripts`
└── rpc.yaml
```
