#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_PACKAGE=${THIS_DIR}/../../packages/yolov5
DIR_DATA=${THIS_DIR}/../../datasets/rpc


pushd ${DIR_PACKAGE} &> /dev/null && \
    export DATA_DIR=~/datasets/rpc && \
    export WANDB_API_KEY=`cat ~/.wandb_api_key` && \
    echo 2 | WANDB_MODE=online \
    python train.py \
        --img 640 --batch 32 --epoch 100 --data ${DIR_DATA}/rpc.yaml \
        --freeze 10 --cache disk --hyp ./data/hyps/hyp.finetune.yaml \
        --weights yolov5x.pt --device 0 && \
    popd
