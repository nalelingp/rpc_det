#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_PACKAGE=${THIS_DIR}/../../packages/yolov5
DIR_DATA=${THIS_DIR}/../../datasets/rpc

export CUDA_NVCC_EXECUTABLE="/usr/local/cuda-11.3/bin/nvcc"
export CUDNN_INCLUDE_PATH="/usr/local/cuda-11.3/include/"
export CUDNN_LIBRARY_PATH="/usr/local/cuda-11.3/lib64/"
export LIBRARY_PATH="/usr/local/cuda-11.3/lib64"

pushd ${DIR_PACKAGE} &> /dev/null && \
    export DATA_DIR=~/datasets/rpc && \
    export WANDB_API_KEY=`cat ~/.wandb_api_key` && \
    echo 2 | WANDB_MODE=online \
    python train.py \
        --img 640 --batch 32 --epoch 100 --data ${DIR_DATA}/rpc.yaml \
        --weights yolov5x.pt --device 0 && \
    popd
