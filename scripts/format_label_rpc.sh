#!/bin/bash
[[ -z ${CONDA_PREFIX} ]] && echo "Conda virtual env is not activated!" && exit

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DIR_DATASET=${THIS_DIR}/../datasets/rpc
[[ ! -d ${DIR_DATASET} ]] && echo "Dataset `rpc` (retail-product-checkout-dataset) does not exist..." && exit 1

DIR_PACKAGES=${THIS_DIR}/../packages
mkdir -p ${DIR_PACKAGES}


echo "Cloning package `JOSN2YOLO...`"
pushd ${DIR_PACKAGES} &> /dev/null && \
    git clone --single-branch --branch master --depth 1 https://github.com/ultralytics/JSON2YOLO.git
    popd

echo "Installing requirements for JSON2YOLO..."
pushd ${DIR_PACKAGES}/JSON2YOLO &> /dev/null && \
    pip install -r requirements.txt
    popd

echo "Converting annotation files to the format for YOLO model..."
pushd ${DIR_PACKAGES}/JSON2YOLO &> /dev/null && \
    python -c "from general_json2yolo import convert_coco_json; convert_coco_json(json_dir='../../datasets/rpc/retail_product_checkout/')" && \
    mv ./new_dir/labels ${DIR_DATASET}/retail_product_checkout/ && \
    rm -rf ./new_dir && \
    popd
