#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
FN_ZIPFILE=${THIS_DIR}/retail-product-checkout-dataset.zip
DIR_OUTPUT=${THIS_DIR}/rpc/retail_product_checkout

mkdir -p ${DIR_OUTPUT}/images


echo "unzipping annotation files..."
unzip -q ${FN_ZIPFILE} \
    instances_train2019.json \
    instances_val2019.json \
    instances_test2019.json \
    -d ${DIR_OUTPUT}

# NOTE: we have to create the folder structure manually to make it meet the standard of MSCOCO dataset
echo "unzipping images..."
unzip -q ${FN_ZIPFILE} \
    train2019/* \
    val2019/* \
    test2019/* \
    -d ${DIR_OUTPUT}/images
