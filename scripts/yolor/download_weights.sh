#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_WEIGHTS=${THIS_DIR}/../../weights/yolor

mkdir -p ${DIR_WEIGHTS}

python -m gdown.cli \
    -O ${DIR_WEIGHTS}/yolor_p6.pt \
    https://drive.google.com/u/0/uc?id=1Tdn3yqpZ79X7R1Ql0zNlNScB1Dv9Fp76
