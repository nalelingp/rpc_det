#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_PACKAGE=${THIS_DIR}/../../packages/yolor
DIR_DATA=${THIS_DIR}/../../datasets/rpc
FN_WEIGHTS=${THIS_DIR}/../../weights/yolor/yolor_p6.pt
# FN_CFG=${THIS_DIR}/../../packages/yolor/cfg/yolor_p6.cfg
FN_CFG=${THIS_DIR}/../../yolor_p6_rpc.cfg


pushd ${DIR_PACKAGE} &> /dev/null && \
    export DATA_DIR=~/datasets/rpc && \
    export WANDB_API_KEY=`cat ~/.wandb_api_key` && \
    echo 2 | WANDB_MODE=disabled \
    python train.py \
        --img 1280 --batch 8 --epoch 5 --data ${DIR_DATA}/rpc.yaml \
        --cfg ${FN_CFG}  --weights ${FN_WEIGHTS} \
        --device 2 && \
    popd
