#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_PACKAGE=${THIS_DIR}/../../packages/yolov4
DIR_DATA=${THIS_DIR}/../../datasets/rpc
FN_WEIGHTS=${THIS_DIR}/../../weights/yolov4/yolov4-pacsp-mish.pt
FN_CFG=${THIS_DIR}/yolov4-pacsp-mish.cfg


# TODO: try to load pre-trained weights

pushd ${DIR_PACKAGE} &> /dev/null && \
    export DATA_DIR=~/datasets/rpc && \
    export WANDB_API_KEY=`cat ~/.wandb_api_key` && \
    echo 2 | WANDB_MODE=online \
    python train.py \
        --img 640 --batch 16 --epoch 100 --data ${DIR_DATA}/rpc.yaml \
        --cfg ${FN_CFG} \
        --hyp data/hyp.scratch.yaml \
        --device 0 --workers 12 && \
    popd
