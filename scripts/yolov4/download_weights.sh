#!/bin/bash
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR_WEIGHTS=${THIS_DIR}/../../weights/yolov4

mkdir -p ${DIR_WEIGHTS}

python -m gdown.cli \
    -O ${DIR_WEIGHTS}/yolov4-pacsp-mish.pt \
    https://drive.google.com/u/0/uc?id=17pQoMfJYbroYqxb6grem2SDY7pZIJPrN
