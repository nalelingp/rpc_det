#!/bin/bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh -b -p ~/miniconda3
~/miniconda3/bin/conda init
rm Miniconda3-latest-Linux-x86_64.sh

touch ~/.condarc
echo "auto_activate_base: false" > ~/.condarc
