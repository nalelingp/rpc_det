## Steps
```bash
$ ./install_conda.sh
$ ./create_env.sh
$ ./install_torch.sh

```

## Other notes
- To launch a new instance of jupyter lab
    - Kill existing instance of jupyter lab if you have already open it through web page
    - Activte your conda virtual environment
    - Execute the script `launch_jupyterlab.sh`. Or you can do it manually:
        ```bash
        # 7000 is the default port open by lambdalabs
        $ jupyter lab --no-browser --port=7000
        ```
    - Then re-open the web page of jupyter lab through lambdalabs, it should be the new instance now
