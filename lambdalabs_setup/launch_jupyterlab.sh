#!/bin/bash
[[ -z ${CONDA_PREFIX} ]] && echo "Conda virtual env is not activated!" && exit

nohup jupyter lab --no-browser &> ~/jupyterlab.log &
