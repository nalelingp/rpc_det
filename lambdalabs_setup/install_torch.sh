#/bin/bash
pip install torch==1.7.1+cu92 \
    torchvision==0.8.2+cu92 \
    torchaudio==0.7.2 \
    -f https://download.pytorch.org/whl/torch_stable.html
